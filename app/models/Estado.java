package models;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by JuanDavid on 09/07/2017.
 */
@Entity
@Table(name = "ncs_estados", schema = "ncs")
public class Estado  extends util.Entity implements Serializable {

    /**
     *  Clase que modela la entididad Estado.
     */
    private int idEstado;
    private String nomEstado;
    private String descEstado;

    @Id
    @Column(name = "ID_ESTADO")
    public int getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(int idEstado) {
        this.idEstado = idEstado;
    }

    @Basic
    @Column(name = "NOM_ESTADO")
    public String getNomEstado() {
        return nomEstado;
    }

    public void setNomEstado(String nomEstado) {
        this.nomEstado = nomEstado;
    }

    @Basic
    @Column(name = "DESC_ESTADO")
    public String getDescEstado() {
        return descEstado;
    }

    public void setDescEstado(String descEstado) {
        this.descEstado = descEstado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Estado estado = (Estado) o;

        if (idEstado != estado.idEstado) return false;
        if (nomEstado != null ? !nomEstado.equals(estado.nomEstado) : estado.nomEstado != null) return false;
        if (descEstado != null ? !descEstado.equals(estado.descEstado) : estado.descEstado != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idEstado;
        result = 31 * result + (nomEstado != null ? nomEstado.hashCode() : 0);
        result = 31 * result + (descEstado != null ? descEstado.hashCode() : 0);
        return result;
    }
}
