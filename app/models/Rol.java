package models;

import util.*;

import javax.persistence.*;
import javax.persistence.Entity;
import java.io.Serializable;

/**
 * Created by JuanDavid on 09/07/2017.
 */
@Entity
@Table(name = "ncs_roles", schema = "ncs")
public class Rol extends util.Entity implements Serializable{

    /**
     *  Clase que modela la entididad Rol.
     */
    private int idRol;
    private int idEstado;
    private String nomRol;
    private String desRol;

    @Id
    @Column(name = "ID_ROL")
    public int getIdRol() {
        return idRol;
    }

    public void setIdRol(int idRol) {
        this.idRol = idRol;
    }

    @Basic
    @Column(name = "ID_ESTADO")
    public int getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(int idEstado) {
        this.idEstado = idEstado;
    }

    @Basic
    @Column(name = "NOM_ROL")
    public String getNomRol() {
        return nomRol;
    }

    public void setNomRol(String nomRol) {
        this.nomRol = nomRol;
    }

    @Basic
    @Column(name = "DES_ROL")
    public String getDesRol() {
        return desRol;
    }

    public void setDesRol(String desRol) {
        this.desRol = desRol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rol rol = (Rol) o;

        if (idRol != rol.idRol) return false;
        if (idEstado != rol.idEstado) return false;
        if (nomRol != null ? !nomRol.equals(rol.nomRol) : rol.nomRol != null) return false;
        if (desRol != null ? !desRol.equals(rol.desRol) : rol.desRol != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idRol;
        result = 31 * result + idEstado;
        result = 31 * result + (nomRol != null ? nomRol.hashCode() : 0);
        result = 31 * result + (desRol != null ? desRol.hashCode() : 0);
        return result;
    }
}
