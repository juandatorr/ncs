package models;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by JuanDavid on 09/07/2017.
 */
@Entity
@Table(name = "ncs_generos", schema = "ncs")
public class Genero  extends util.Entity implements Serializable {

    /**
     *  Clase que modela la entididad Genero musical.
     */
    private int idGenero;
    private int idEstado;
    private String nomGenero;
    private String descGenero;

    @Id
    @Column(name = "ID_GENERO")
    public int getIdGenero() {
        return idGenero;
    }

    public void setIdGenero(int idGenero) {
        this.idGenero = idGenero;
    }

    @Basic
    @Column(name = "ID_ESTADO")
    public int getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(int idEstado) {
        this.idEstado = idEstado;
    }

    @Basic
    @Column(name = "NOM_GENERO")
    public String getNomGenero() {
        return nomGenero;
    }

    public void setNomGenero(String nomGenero) {
        this.nomGenero = nomGenero;
    }

    @Basic
    @Column(name = "DESC_GENERO")
    public String getDescGenero() {
        return descGenero;
    }

    public void setDescGenero(String descGenero) {
        this.descGenero = descGenero;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Genero genero = (Genero) o;

        if (idGenero != genero.idGenero) return false;
        if (idEstado != genero.idEstado) return false;
        if (nomGenero != null ? !nomGenero.equals(genero.nomGenero) : genero.nomGenero != null) return false;
        if (descGenero != null ? !descGenero.equals(genero.descGenero) : genero.descGenero != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idGenero;
        result = 31 * result + idEstado;
        result = 31 * result + (nomGenero != null ? nomGenero.hashCode() : 0);
        result = 31 * result + (descGenero != null ? descGenero.hashCode() : 0);
        return result;
    }
}
