package models;

import org.hibernate.Criteria;
import org.hibernate.Session;
import play.db.jpa.JPA;

import javax.persistence.*;
import javax.xml.transform.Result;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by JuanDavid on 09/07/2017.
 */
@Entity
@Table(name = "ncs_discos", schema = "ncs")
public class Disco extends util.Entity implements Serializable {

    /**
     *  Clase que modela la entididad Disco.
     */
    private int idDisco;
    private int idEstado;
    private String correoElectronico;
    private String nomDisco;
    private Timestamp fLanzamiento;
    private String descDisco;

    @Id
    @Column(name = "ID_DISCO")
    public int getIdDisco() {
        return idDisco;
    }

    public void setIdDisco(int idDisco) {
        this.idDisco = idDisco;
    }

    @Basic
    @Column(name = "ID_ESTADO")
    public int getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(int idEstado) {
        this.idEstado = idEstado;
    }

    @Basic
    @Column(name = "CORREO_ELECTRONICO")
    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    @Basic
    @Column(name = "NOM_DISCO")
    public String getNomDisco() {
        return nomDisco;
    }

    public void setNomDisco(String nomDisco) {
        this.nomDisco = nomDisco;
    }

    @Basic
    @Column(name = "F_LANZAMIENTO")
    public Timestamp getfLanzamiento() {
        return fLanzamiento;
    }

    public void setfLanzamiento(Timestamp fLanzamiento) {
        this.fLanzamiento = fLanzamiento;
    }

    @Basic
    @Column(name = "DESC_DISCO")
    public String getDescDisco() {
        return descDisco;
    }

    public void setDescDisco(String descDisco) {
        this.descDisco = descDisco;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Disco disco = (Disco) o;

        if (idDisco != disco.idDisco) return false;
        if (idEstado != disco.idEstado) return false;
        if (correoElectronico != null ? !correoElectronico.equals(disco.correoElectronico) : disco.correoElectronico != null)
            return false;
        if (nomDisco != null ? !nomDisco.equals(disco.nomDisco) : disco.nomDisco != null) return false;
        if (fLanzamiento != null ? !fLanzamiento.equals(disco.fLanzamiento) : disco.fLanzamiento != null) return false;
        if (descDisco != null ? !descDisco.equals(disco.descDisco) : disco.descDisco != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idDisco;
        result = 31 * result + idEstado;
        result = 31 * result + (correoElectronico != null ? correoElectronico.hashCode() : 0);
        result = 31 * result + (nomDisco != null ? nomDisco.hashCode() : 0);
        result = 31 * result + (fLanzamiento != null ? fLanzamiento.hashCode() : 0);
        result = 31 * result + (descDisco != null ? descDisco.hashCode() : 0);
        return result;
    }

    public static List discosArtista(String correoElectronico){

        List entity = null;
        Session session = (Session) JPA.em().getDelegate();
        try {
            entity = session.createSQLQuery("SELECT * FROM ncs_discos where CORREO_ELECTRONICO="+ correoElectronico +"")
                    .setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();
            return entity;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int actualizarDisco(int idDisco){
        int entity = 0;
        Session session = (Session) JPA.em().getDelegate();
        try {
            entity = session.createSQLQuery("UPDATE ncs_discos SET  ID_ESTADO = 2 WHERE ID_DISCO = "+idDisco+"").executeUpdate();
            return entity;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entity;
    }
}
