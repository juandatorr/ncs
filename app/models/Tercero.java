package models;

import org.hibernate.Session;
import play.db.jpa.JPA;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by JuanDavid on 09/07/2017.
 */
@Entity
@Table(name = "ncs_tercero", schema = "ncs")
public class Tercero  extends util.Entity implements Serializable {

    /**
     *  Clase que modela la entididad Tercero.
     */
    private String correoElectronico;
    private int idEstado;
    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;
    private String direccion;
    private String numCelular;

    @Id
    @Column(name = "CORREO_ELECTRONICO")
    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    @Basic
    @Column(name = "ID_ESTADO")
    public int getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(int idEstado) {
        this.idEstado = idEstado;
    }

    @Basic
    @Column(name = "PRIMER_NOMBRE")
    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    @Basic
    @Column(name = "SEGUNDO_NOMBRE")
    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    @Basic
    @Column(name = "PRIMER_APELLIDO")
    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    @Basic
    @Column(name = "SEGUNDO_APELLIDO")
    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    @Basic
    @Column(name = "DIRECCION")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "NUM_CELULAR")
    public String getNumCelular() {
        return numCelular;
    }

    public void setNumCelular(String numCelular) {
        this.numCelular = numCelular;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tercero tercero = (Tercero) o;

        if (idEstado != tercero.idEstado) return false;
        if (correoElectronico != null ? !correoElectronico.equals(tercero.correoElectronico) : tercero.correoElectronico != null)
            return false;
        if (primerNombre != null ? !primerNombre.equals(tercero.primerNombre) : tercero.primerNombre != null)
            return false;
        if (segundoNombre != null ? !segundoNombre.equals(tercero.segundoNombre) : tercero.segundoNombre != null)
            return false;
        if (primerApellido != null ? !primerApellido.equals(tercero.primerApellido) : tercero.primerApellido != null)
            return false;
        if (segundoApellido != null ? !segundoApellido.equals(tercero.segundoApellido) : tercero.segundoApellido != null)
            return false;
        if (direccion != null ? !direccion.equals(tercero.direccion) : tercero.direccion != null) return false;
        if (numCelular != null ? !numCelular.equals(tercero.numCelular) : tercero.numCelular != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = correoElectronico != null ? correoElectronico.hashCode() : 0;
        result = 31 * result + idEstado;
        result = 31 * result + (primerNombre != null ? primerNombre.hashCode() : 0);
        result = 31 * result + (segundoNombre != null ? segundoNombre.hashCode() : 0);
        result = 31 * result + (primerApellido != null ? primerApellido.hashCode() : 0);
        result = 31 * result + (segundoApellido != null ? segundoApellido.hashCode() : 0);
        result = 31 * result + (direccion != null ? direccion.hashCode() : 0);
        result = 31 * result + (numCelular != null ? numCelular.hashCode() : 0);
        return result;
    }

    public static int actualizarTercero(String correo, String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, String direccion, String numCelular){
        int entity = 0;
        Session session = (Session) JPA.em().getDelegate();
        try {
            entity = session.createSQLQuery("UPDATE ncs_tercero SET PRIMER_NOMBRE = "+primerNombre+", SEGUNDO_NOMBRE = "+segundoNombre+", PRIMER_APELLIDO = "+primerApellido+", SEGUNDO_APELLIDO = "+segundoApellido+", " +
                    " DIRECCION = "+direccion+", NUM_CELULAR = "+numCelular+" WHERE CORREO_ELECTRONICO = "+correo+"").executeUpdate();
            return entity;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entity;
    }

    public static int eliminarTercero(String correo){
        int entity = 0;
        Session session = (Session) JPA.em().getDelegate();
        try {
            entity = session.createSQLQuery("UPDATE ncs_tercero SET  ID_ESTADO = 2 WHERE CORREO_ELECTRONICO = "+correo+"").executeUpdate();
            return entity;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entity;
    }
}
