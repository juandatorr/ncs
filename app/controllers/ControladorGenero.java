package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Genero;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

/**
 * Created by JuanDavid on 08/07/2017.
 */
public class ControladorGenero extends Controller {

    @Transactional
    public Result agregarGenero() {
        JsonNode json = request().body().asJson();
        Genero genero = Json.fromJson(json, Genero.class);
        if (genero.getIdGenero() != 0) {
            genero.save();
        }else{
            return ok("Error");
        }
        return ok("Exito");
    }

    @Transactional
    public Result eliminarGenero(Long idCancion){
        Genero genero = (Genero) Genero.findById(Genero.class, idCancion);
        genero.delete();
        return ok(Json.toJson("Elimino"));
    }

    @Transactional
    public Result actualizarGenero(Long idCancion){
        Genero g = (Genero) Genero.findById(Genero.class, idCancion);
        if(g != null){
            JsonNode json = request().body().asJson();
            Genero genero = Json.fromJson(json, Genero.class);
            genero.update();
        }else{
            return ok("Error");
        }
        return ok("Exito");

    }

    @Transactional
    public Result listaGeneros(){
        List genero =  Genero.findAll(Genero.class);
        return ok(Json.toJson(genero));
    }
}
