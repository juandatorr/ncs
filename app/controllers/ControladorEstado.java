package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Estado;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

/**
 * Created by JuanDavid on 08/07/2017.
 */
public class ControladorEstado extends Controller {

    @Transactional
    public Result agregarEstado() {
        JsonNode json = request().body().asJson();
        Estado estado = Json.fromJson(json, Estado.class);
        if (estado.getIdEstado() != 0) {
            estado.save();
        }else{
            return ok("Error");
        }
        return ok("Exito");
    }

    @Transactional
    public Result eliminarEstado(Long idCancion){
        Estado estado = (Estado) Estado.findById(Estado.class, idCancion);
        estado.delete();
        return ok(Json.toJson("Elimino"));
    }

    @Transactional
    public Result actualizarEstado(Long idCancion){
        Estado e = (Estado) Estado.findById(Estado.class, idCancion);
        if(e != null){
            JsonNode json = request().body().asJson();
            Estado estado = Json.fromJson(json, Estado.class);
            estado.update();
        }else{
            return ok("Error");
        }
        return ok("Exito");

    }

    @Transactional
    public Result listaEstados(){
        List estado = Estado.findAll(Estado.class);
        return ok(Json.toJson(estado));
    }
}
