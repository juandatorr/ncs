package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Rol;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

/**
 * Created by JuanDavid on 08/07/2017.
 */
public class ControladorRol extends Controller {

    @Transactional
    public Result agregarRol() {
        JsonNode json = request().body().asJson();
        Rol rol = Json.fromJson(json, Rol.class);
        if (rol.getIdRol() == 0) {
            rol.save();
        }else{
            return ok("Error");
        }
        return ok("Exito");
    }

    @Transactional
    public Result eliminarRol(Long idRol){
        Rol rol = (Rol) Rol.findById(Rol.class, idRol);
        rol.delete();
        return ok(Json.toJson("Elimino"));
    }

    @Transactional
    public Result actualizarRol(Long idRol){
        Rol r = (Rol) Rol.findById(Rol.class, idRol);
        if(r != null){
            JsonNode json = request().body().asJson();
            Rol rol = Json.fromJson(json, Rol.class);
            rol.update();
        }else{
            return ok("Error");
        }
        return ok("Exito");

    }

    @Transactional
    public Result listaRoles(){
        List rol =  Rol.findAll(Rol.class);
        return ok(Json.toJson(rol));
    }
}
