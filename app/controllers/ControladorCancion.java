package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Cancion;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import java.io.File;
import java.sql.Blob;
import java.util.List;

/**
 * Created by JuanDavid on 08/07/2017.
 */
public class ControladorCancion extends Controller{

    @Transactional
    public Result agregarCancion() {
        JsonNode json = request().body().asJson();
        System.out.println(json);
        Cancion cancion = Json.fromJson(json, Cancion.class);
        if (cancion.getIdMusica() != 0) {
            cancion.save();
        }else{
            return ok("Error");
        }
        return ok("Exito");
    }

    @Transactional
    public Result eliminarCancion(Long idCancion){
        Cancion cancion = (Cancion) Cancion.findById(Cancion.class, idCancion);
        cancion.delete();
        return ok(Json.toJson("Elimino"));
    }

    @Transactional
    public Result actualizarCancion(Long idCancion){
        Cancion c = (Cancion) Cancion.findById(Cancion.class, idCancion);
        if(c != null){
            JsonNode json = request().body().asJson();
            Cancion cancion = Json.fromJson(json, Cancion.class);
            cancion.update();
        }else{
            return ok("Error");
        }
        return ok("Exito");

    }

    @Transactional
    public Result listaCancion(){
        List cancion = Cancion.findAll(Cancion.class);
        return ok(Json.toJson(cancion));
    }

}
