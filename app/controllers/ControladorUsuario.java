package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Usuario;
import play.db.jpa.Transactional;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

/**
 * Created by JuanDavid on 08/07/2017.
 */
public class ControladorUsuario extends Controller {

    @Transactional
    public Result agregarUsuario() {
        JsonNode json = request().body().asJson();
        Usuario usuario = Json.fromJson(json, Usuario.class);
        if (usuario.getCorreoElectronico() != null) {
            usuario.save();
        }else{
            return ok("Error");
        }
        return ok("Exito");
    }

    @Transactional
    public Result eliminarUsuario(String correo){
        Usuario usuario = (Usuario) Usuario.findByIdString(Usuario.class, correo);
        usuario.delete();
        return ok(Json.toJson("Elimino"));
    }

    @Transactional
    public Result actualizarUsuario(String correo){
            JsonNode json = request().body().asJson();
            int entity = 0;
            String clave = json.path("valor").path("clave").toString();
            entity = Usuario.actualizarUsuario(correo, clave);
            if (entity != 0) {
                return ok(Json.toJson("Exito"));
            }
            return ok(Json.toJson("Error"));
        }


    @Transactional
    public Result listaUsuarios(){
        List usuario = Usuario.findAll(Usuario.class);
        return ok(Json.toJson(usuario));
    }

    @Transactional()
    public Result login(String correoElectronico, String clave) {
        Object entity = null;
        try {
            entity= Usuario.ingresarUsuario(correoElectronico, clave);
            return ok(Json.toJson(entity));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ok(Json.toJson("error"));
    }

    @Transactional
    public Result darClave(String correoElectronico){
        Usuario usuario = (Usuario) Usuario.findByIdString(Usuario.class, correoElectronico);
        return ok(Json.toJson(usuario));
    }

}
